import React from 'react';
import Payment from './pages/Payment/';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const App = () => (
  <div>
    <Router>
      <Switch>
        <Route path="/payment">
          <Payment />
        </Route>
        <Route path="/">
          <Payment />
        </Route>
      </Switch>
    </Router>
  </div>
);


export default App;
