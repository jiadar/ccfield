import { SET_CARD_TYPE,
         CLEAR_CARD_TYPE,
         UPDATE_CARD_DATA,
         SHOW_TOAST,
         API_ERROR,
       } from './actionTypes';
         
export default function reducer(state, action) {
  switch (action.type) {
  case SET_CARD_TYPE:
    return { ...state, ...action.payload };
  case CLEAR_CARD_TYPE:
    return { ...state, cardType: "" };
  case UPDATE_CARD_DATA:
    return { ...state, ...action.payload };
  case SHOW_TOAST:
    return { ...state, showToast: true };
  case API_ERROR:
    return { ...state, apiError: true };
  default:
    throw new Error();
  }
}
