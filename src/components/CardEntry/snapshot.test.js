import React from 'react';
import renderer from 'react-test-renderer';
import CardEntry from './index';

describe('CardEntry', () => {
  test('snapshot without props', () => {
    const component = renderer.create(<CardEntry />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  test('snapshot with placeholder prop', () => {
    const component = renderer.create(<CardEntry placeholder="1234 1234 1234 1234" />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
