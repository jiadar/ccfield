import React from 'react';
import reducer from './reducer'
import { clearCardType, apiError, showToast, setCardType, updateCardData } from './actionCreators';
import { cardIsValid, inputIsNotFullLength, getCardData } from './utils';

describe('Reducer', () => {
  it('should set a card type to visa', () => {
    const state = {};
    const newState = reducer(state, setCardType("visa"));
    expect(newState).toEqual({ cardType: "visa" });
  });
  it('should clear a card type', () => {
    const state = { cardType: "visa" };
    const newState = reducer(state, clearCardType());
    expect(newState).toEqual({ cardType: "" });
  });
  it('should show a toast', () => {
    const state = { showToast: false };
    const newState = reducer(state, showToast());
    expect(newState).toEqual({ showToast: true });
  });
  it('should update card data', () => {
    const state = { cardNumber: "", formattedCardNumber: "", cardValid: false };
    const data = getCardData("431332", "visa")
    const newState = reducer(state, updateCardData(data));
    expect(newState).toEqual({ cardNumber: "431332", formattedCardNumber: "4313 32", cardValid: false });
  });
  it('should set the API error', () => {
    const state = { apiError: false };
    const newState = reducer(state, apiError());
    expect(newState).toEqual({ apiError: true });
  });
  it('should determine a card with only numbers is valid', () => {
    const returnValue = cardIsValid("4313322344395342", 16);
    expect(returnValue).toEqual(true);
  });
  it('should determine a card with anything but numbers is not valid', () => {
    const returnValue = cardIsValid("43133223b4395342", 16);
    expect(returnValue).toEqual(false);
  });
  it('should determine a card without the required number of digits is invalid', () => {
    const returnValue = cardIsValid("4313322439542", 15);
    expect(returnValue).toEqual(false);
  });
  it('should determine the input is full length at 15 for amex', () => {
    const returnValue = inputIsNotFullLength("372332233456331", "amex");
    expect(returnValue).toEqual(false);
  });
  it('should determine the input is full length at 16 for visa', () => {
    const returnValue = inputIsNotFullLength("3723322433456331", "visa");
    expect(returnValue).toEqual(false);
  });  
  it('should determine the input is not at full length amex', () => {
    const returnValue = inputIsNotFullLength("37332233456331", "amex");
    expect(returnValue).toEqual(true);
  });  
  it('should determine the input is not at full length visa', () => {
    const returnValue = inputIsNotFullLength("372332243456331", "visa");
    expect(returnValue).toEqual(true);
  });  
});

