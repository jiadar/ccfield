import { clearCardType, setCardType } from './actionCreators'

export const binlookup = (prefix) => {
  if (prefix.substring(0, 1) === "4") return "visa";
  if (prefix.substring(0, 1) === "5") return "mastercard";
  if (prefix.substring(0, 2) === "37") return "amex";
  if (prefix.substring(0, 2) === "38") return "amex";
}

export const lookupCardType = (nextCardNumber, dispatch) => {
  if (nextCardNumber.length < 4) dispatch(clearCardType());
  if (nextCardNumber.length !== 4) return;
  const scheme = binlookup(nextCardNumber);
  dispatch(setCardType(scheme));
}

export const getCardData = (nextCardNumber, cardType) => {
  var formattedCardNumber = "";
  for (var i = 0; i < nextCardNumber.length; i++) {
    if ( i < maxCardLength(cardType, i) ) formattedCardNumber += nextCardNumber.charAt(i)
    if ( shouldAddSpace(cardType, i) ) formattedCardNumber += " ";
  }
  const cardValid = cardIsValid(nextCardNumber, maxCardLength(cardType));
  return { cardNumber: nextCardNumber, formattedCardNumber, cardValid };    
}

export const inputIsNotFullLength = (cardNumber, cardType) => {
  if (cardNumber.length === 15 && cardType === "amex") return false;
  if (cardNumber.length === 16) return false;
  return true;
}

export const cardIsValid = (nextCardNumber, len) => {
  if (/[^0-9]/.test(nextCardNumber)) return false;
  if (nextCardNumber.length !== len) return false;
  return true;
}


const shouldAddSpace = (cardType, i) => {
  if (cardType === "amex") return (i === 3 || i === 9);
  return (i === 3 || i === 7 || i === 11);
}

const maxCardLength = (cardType, i) => {
  if (cardType === "amex") return 15;
  return 16;
}


