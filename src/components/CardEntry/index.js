import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
import { InputContainer, Input, Toast, Error } from './styles';
import { lookupCardType, getCardData, inputIsNotFullLength } from './utils';
import { updateCardData, showToast } from './actionCreators';
import checkIcon from '../../images/check.svg';
import xIcon from '../../images/x.svg';

import reducer from './reducer';

function CardEntry(props) {

  const initialState = {
    cardNumber: "",
    placeholder: props.placeholder,
    formattedCardNumber: "",
    cardValid: false,
    showToast: false,
    apiError: false,
    cardType: "",
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const handleKeyPress = (event) => {
    event.preventDefault();
    const nextCardNumber = (event.key === 'Backspace')
          ?  state.cardNumber.slice(0, -1)
          : (event.key.length === 1 && inputIsNotFullLength(state.cardNumber, state.cardType))
          ? state.cardNumber + event.key
          : state.cardNumber;
    lookupCardType(nextCardNumber, dispatch);
    const updatedCardData = getCardData(nextCardNumber, state.cardType);
    dispatch(updateCardData(updatedCardData));
  }                           

  const handleBlur = (event) => {
    dispatch(showToast());
  }

  const noop = () => {};

  return (
    <form className="pa4 black-80" onSubmit={noop}>
      <label className="f5 db mb2">Credit Card Number
        <span className="red f6"><sup>&nbsp;*</sup></span>
      </label>
      <InputContainer>
        <Input id="ccnumber"
               type="text"
               onChange={noop}
               placeholder={state.placeholder}
               value={state.formattedCardNumber}
               onKeyDown={handleKeyPress}
               onBlur={handleBlur}
               cardType={state.cardType}
        />
        { state.showToast && !state.cardValid && <Toast src={xIcon} /> }
        { state.showToast && state.cardValid && <Toast src={checkIcon} /> }
      </InputContainer>
      { state.apiError && <Error>Oh no! Something went wrong.</Error> }
    </form>
  )
};

CardEntry.propTypes = {
    placeholder: PropTypes.string,  
}

CardEntry.defaultProps = {
    placeholder: '',  
}

export default CardEntry;
