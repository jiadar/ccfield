import styled from 'styled-components';
import visaImage from '../../images/visa.svg';
import amexImage from '../../images/amex.svg';
import masterImage from '../../images/mastercard.svg';

export const InputContainer = styled.div.attrs(props => ({
  className: 'flex pa0 mb2 db'
}))`
`;

export const Input = styled.input.attrs(props => ({
  className: 'input-reset ba b--black-20 pa2 mb2 db'
}))`
  height: 35px;
  width: 350px;
  color: #282828;
  ${({ cardType }) => cardType === "visa" && `background: url(${visaImage});`}
  ${({ cardType }) => cardType === "amex" && `background: url(${amexImage});`}
  ${({ cardType }) => cardType === "mastercard" && `background: url(${masterImage});`}
  background-repeat: no-repeat;
  background-position: 310px 3px;
  background-size: 28px 28px
`;

export const Toast = styled.img.attrs(props => ({
  className: 'pl2 pr0 pb0 pt0 mb0'
}))`
`;

export const Error = styled.div.attrs(props => ({
  className: 'pa2 mb2 db light-red b--light-red ba'
}))`
  position:relative;
  height: 35px;
  width: 370px;
`;

