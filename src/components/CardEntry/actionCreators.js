import { SET_CARD_TYPE,
         CLEAR_CARD_TYPE,
         UPDATE_CARD_DATA,
         SHOW_TOAST,
         API_ERROR,
       } from './actionTypes';

export const clearCardType = () => {
  return { type: CLEAR_CARD_TYPE }
}

export const apiError = () => {
  return { type: API_ERROR }
}

export const showToast = () => {
  return { type: SHOW_TOAST }
}

export const setCardType = (cardType) => {
  return {
    type: SET_CARD_TYPE,
    payload: { cardType }
  }
}

export const updateCardData = (cardData) => {
  return {
    type: UPDATE_CARD_DATA,
    payload: cardData
  }
}
