export const SET_CARD_TYPE = 'SET_CARD_TYPE';
export const CLEAR_CARD_TYPE = 'CLEAR_CARD_TYPE';
export const UPDATE_CARD_DATA = 'UPDATE_CARD_DATA';
export const SHOW_TOAST = 'SHOW_TOAST';
export const API_ERROR = 'API_ERROR';

