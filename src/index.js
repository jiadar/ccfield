import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppContent from './App';
import * as serviceWorker from './serviceWorker';


const App = () => (
  <AppContent />
);

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
