---
title: Credit Card Field Demo Documentation

language_tabs: # must be one of https://git.io/vQNgJ
  - javascript

toc_footers:
  - Content by <a href='http://www.javin.io'>Javin Lacson</a>
  - Questions? <a href='mailto:work@javin.io'>email me</a>

search: true
---

# Introduction

This is a demo of a credit card field, including state, validation, and tests. The demo uses an ejected Create React App, Tachyons and Styled Components for CSS, and the react reducers hook. 

The entry is `index.js`. Routing is in `App.js`. A payment page is rendered by App, which renders a CardEntry component with placeholder text.

The bulk of the demo is in the CardEntry component. It contains the following:

File|Description
------|-----
**index.js** | The CardEntry react component
**styles.js** | CSS as react components
**utils.js** | Utilities for calculating validation and formatting logic
**actionTypes.js** | Action type contants for state management
**actionCreators.js** | Action creator functions for state management
**reducer.js** | Reducer for state change management
**unit.test.js** | Unit tests for testing validation and formatting logic as well as state changes
**snapshot.test.js** | Snapshot tests for testing changes to rendered page and props
 
# CardEntry Component

This component renders a controlled field for input. Depending on the state, it will also render a toast or an API Error. Because we don't have any submit for the form, we just call a noop on submit. 

## State
```javascript
const [state, dispatch] = useReducer(reducer, initialState);
```
To keep state we use the reducer hook from React, provided with a sane initial state. We can accept placeholder text from a prop. 

## Keypress
Catch the keypress operations so that we can also implement *delete*. Arrow keys and other special keys are ignored for the sake of the demo. To do this, we call `preventDefault()`. The main function in the keypress callback is to compute the next card number based on the keypress input. 

```javascript
const nextCardNumber = (event.key === 'Backspace')
  ?  state.cardNumber.slice(0, -1)
  : (event.key.length === 1 && inputIsNotFullLength(state.cardNumber, state.cardType))
  ? state.cardNumber + event.key
  : state.cardNumber;
```

To calculate the next card number, we first see if the key is backspace. If it was a backspace, we will slice off the final character of the current card number. If it wasn't a backspace, we check if the key is not a special character - in this case it will have a length of 1. We also check if the card number has not reached the full length - if it hasn't we can go ahead and add the key. If we have reached the full length, we won't allow any more input and return the current state. 

```javascript
lookupCardType(nextCardNumber, dispatch);
const updatedCardData = getCardData(nextCardNumber, state.cardType);
dispatch(updateCardData(updatedCardData));
```

Finally, we look up the card type. This could potentially trigger an API network call. Pass it the dispatch function so that it can update the state asyncronously. We get the updated card data and dispatch an update to the card data in local state.

## Blur

```javascript
const handleBlur = (event) => {
  dispatch(showToast());
}
```
If we move focus away from the field, we will show a check or x depending on if the field is validated or not.

```javascript
{ state.showToast && !state.cardValid && <Toast src={xIcon} /> }
{ state.showToast && state.cardValid && <Toast src={checkIcon} /> }
```

We show the appropriate toast in the render method conditionally.

# Utils

We provide a number of utilities to abstract the functions related to field validation and formatting.

## lookupCardType

```javascript
export const lookupCardType = (nextCardNumber, dispatch) => {
  if (nextCardNumber.length < 4) dispatch(clearCardType());
  if (nextCardNumber.length !== 4) return;
  binlookup(nextCardNumber)
    .then(data => dispatch(setCardType(data.scheme)))
    .catch(err => dispatch(apiError()));
}
```
This utility calls an API to determine the card type. We run this call if the length is 4. If the length drops below 4, we clear the card type. After the API call executes, we either set the card type or an error.

## getCardData

```javascript
export const getCardData = (nextCardNumber, cardType) => {
  var formattedCardNumber = "";
  for (var i = 0; i < nextCardNumber.length; i++) {
    if ( i < maxCardLength(cardType, i) ) formattedCardNumber += nextCardNumber.charAt(i)
    if ( shouldAddSpace(cardType, i) ) formattedCardNumber += " ";
  }
  const cardValid = cardIsValid(nextCardNumber, maxCardLength(cardType));
  return { cardNumber: nextCardNumber, formattedCardNumber, cardValid };    
}
```  
Given a next card number and type, we produce formatted data, the card number string (unformatted), and determine the validity of the card.

## inputIsNotFullLength

```javascript
export const inputIsNotFullLength = (cardNumber, cardType) => {
  if (cardNumber.length === 15 && cardType === "amex") return false;
  if (cardNumber.length === 16) return false;
  return true;
}
```
             
Given a card number and card type, determine if the input is not full length.

## cardIsValid

```javascript
export const cardIsValid = (nextCardNumber, len) => {
  if (/[^0-9]/.test(nextCardNumber)) return false;
  if (nextCardNumber.length !== len) return false;
  return true;
}
```
Given a card number and required length, determine if it is valid. It is valid if it contains only digits, and matches the length. 

## shouldAddSpace

```javascript
const shouldAddSpace = (cardType, i) => {
  if (cardType === "amex") return (i === 3 || i === 9);
  return (i === 3 || i === 7 || i === 11);
}
```
Given a card type and a loop index, determine if we should add a space. The formatting is different for Amex vs Visa/MC.

## maxCardLength

```javascript
const maxCardLength = (cardType, i) => {
  if (cardType === "amex") return 15;
  return 16;
}
```

Returns the max card length, 15 for amex, or 16 for others.

# State

```javascript
export default function reducer(state, action) {
  switch (action.type) {
  case SET_CARD_TYPE:
    return { ...state, ...action.payload };
  case CLEAR_CARD_TYPE:
    return { ...state, cardType: "" };
  case UPDATE_CARD_DATA:
    return { ...state, ...action.payload };
  case SHOW_TOAST:
    return { ...state, showToast: true };
  case API_ERROR:
    return { ...state, apiError: true };
  default:
    throw new Error();
  }
}
```    
State is implemented using the react reducer hook. The reducer is pretty simple. Action creators are also utilized to make the code more readable. 
    
# Testing

```javascript
it('should update card data', () => {
  const state = { cardNumber: "", formattedCardNumber: "", cardValid: false };
  const data = getCardData("431332", "visa")
  const newState = reducer(state, updateCardData(data));
  expect(newState).toEqual({ cardNumber: "431332", formattedCardNumber: "4313 32", cardValid: false });
});
```    
Jest is used for testing. Unit tests are in `unit.test.js` and snapshot tests are in `snapshot.test.js`. A handful of tests were written quickly, and do not cover nearly all cases. Run the tests with `npm run test`. Here's an example of one of the unit tests for the reducer. 
    
